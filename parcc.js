// proxy, popup, plugin settings for parcc test
// http://kb.mozillazine.org/About:config_entries
// turn off first run screen
lockPref("pdfjs.firstRun", false);
lockPref("browser.shell.checkDefaultBrowser", false);

// Allow plugin popups
lockPref("privacy.popups.disable_from_plugins", 1);
lockPref("dom.disable_open_during_load", false);


// Check for plugins

lockPref("plugin.scan.SunJRE", "1.6.3");
lockPref("plugin.scan.Java", "8.25");

// allow java from blocklist 
lockPref("security.enable_java", 1);
lockPref("plugin.state.java", 2);

lockPref("extensions.blocklist.enabled", true);
lockPref("extensions.blocklist.url", "");

//lockPref("extensions.blocklist.level", 2);

// Turn off auto-updates, reports
lockPref("app.update.auto", false);
lockPref("toolkit.telemetry.enabled", false);
lockPref("datareporting.healthreport.service.enabled", false);

// disable password services
lockPref("privacy.cpd.passwords", false);
lockPref("privacy.clearOnShutdown.passwords", true);
lockPref("services.sync.engine.passwords", false);
lockPref("signon.rememberSignons", false);

// Allow full screen
lockPref("full-screen-api.approval-required", false);